<div id="form_input">
        <div class="row">
           <div class="input-field col s6">
                <input id="first_name" type="text" name="firstname" class="validate" required>
                <label for="first_name">First Name</label>
            </div>
            <div class="input-field col s6">
                <input id="last_name" type="text" name="lastname" class="validate" required>
                <label for="last_name">Last Name</label>
            </div>

              <div class="input-field col s6">
                <input type="date" id="birthdate" name="birthdate" class="datepicker" required>
                <label for="birthdate">Birthdate</label>
            </div>

             <div class="input-field col s6">
                <input id="email" type="email" name="email" class="validate" required>
                <label for="email">Email</label>
            </div>
           
          
        </div>
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
</div>