<?php include_once '../includes/connect.php'; ?>



<?php
	if($_POST){
		$sql = "SELECT * FROM user_types WHERE type_name like '%".$_POST['search']."%'";
	}else{
		$sql = "SELECT * FROM user_types";
	}


	$result = mysqli_query($conn, $sql);



	while($row = mysqli_fetch_assoc($result)){ ?>

		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['type_name']; ?></td>

			<td>

	            <a class="btn-floating btn-large waves-effect waves-light green edit btn modal-trigger" data-target="user<?php echo $row['id']; ?>"><i class="material-icons">edit</i></a>
	            <a class="btn-floating btn-large waves-effect waves-light red delete" href="delete.php?id=<?php echo $row['id']; ?>"><i class="material-icons">delete</i></a>
				<div id="user<?php echo $row['id'];?>" class="modal modal-fixed-footer">
						<div class="modal-content">
							 <h4> <i class="large material-icons">mode_edit</i></h4>
						<hr>
						<form class="edit_form">
                        <div class="edit_form_input">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input  type="text" placeholder="Type Name" name="type_name" value="<?php echo $row['type_name']; ?>" class="validate"  required>
                                </div>
                                 <input type="hidden" value="<?php echo $row['id']; ?>" name="id">

                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
					</div>
					<div class="modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
					</div>
				</div>
            </td>

            	
		

<?php
	}



?>


<script type="text/javascript">
	$(document).ready(function(){
		var $form1 = $(".edit_form").html();
		$('.modal-trigger').leanModal();
		
	});
</script>