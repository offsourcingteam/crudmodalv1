<?php include "../includes/header.php" ?>


         <!-- Modal Trigger -->
          <!-- Modal Trigger --> <!-- Modal Trigger -->
        <div class="row">
            <div class="input-field col s8">
                <a class="modal-trigger btn-floating btn-large waves-effect waves-light red" href="#modal1"><i class="material-icons">add</i></a>
            </div>
            <form id="search_form">
                <div class="input-field col s4">
                  <input id="search" type="text" name="search" class="validate">
                  <label for="search">Search</label>
                </div>
            </form>
        </div>
        

        
    
          

        <table class="bordered">
            <thead>
                <tr>
                    <th data-field="id">ID</th>
                    <th data-field="first_name">First Name</th>
                    <th data-field="last_name">Last Name</th>
                    <th data-field="birthdate">Birthdate</th>
                    <th data-field="email">Email</th>
                    <th data-field="email">User Type</th>
                    <th data-field="actions">Actions</th>
                </tr>
            </thead>

            <tbody id="result">
            </tbody>
        </table>
    </div>
  <!--   <div class="container">
        
    </div> -->

<!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4> <i class="large material-icons">perm_identity</i></h4>
            <div class="row">
                <hr>
                    <form id="add_form">
                        <div id="form_input">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="first_name" type="text" name="firstname" class="validate" required>
                                    <label for="first_name">First Name</label>
                                </div>

                                <div class="input-field col s6">
                                    <input id="last_name" type="text" name="lastname" class="validate" required>
                                    <label for="last_name">Last Name</label>
                                </div>

                                <div class="input-field col s6">
                                    <?php 
                                        include_once '../includes/connect.php'; 
                                        $sql_user_type = "SELECT * FROM user_types";
                                        $user_type_result = mysqli_query($conn, $sql_user_type);
                                    ?>
                                    <select name="user_type_id">
                                        <?php while($row_user_type = mysqli_fetch_assoc($user_type_result)){  ?>
                                            <option value="<?php echo $row_user_type['id'] ?>"><?php echo $row_user_type['type_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <label>User Type</label>
                                </div>

                                <div class="input-field col s6">
                                    <input type="date" id="birthdate" name="birthdate" class="datepicker" required>
                                    <label for="birthdate">Birthdate</label>
                                </div>

                                <div class="input-field col s6">
                                    <input id="email" type="email" name="email" class="validate" required>
                                    <label for="email">Email</label>
                                </div>


                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
        </div>
    
    <?php include "../includes/footer.php" ?>

    <script type="text/javascript">

            $(document).ready(function(){
                loadUsers();
                var $form1 = $("#add_form").html();
                var $form2 = $(".edit_form").html();

                $("#add_form").submit(function(e){
                    e.preventDefault();
                    $.ajax({
                        url: "add.php",
                        type: "POST",
                        data: $(this).serialize(),
                        success: function(msg){
                            $("#add_form").html(msg);
                        }
                    });
                });


                // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
                $('.modal-trigger').leanModal({ // Callback for Modal open
                    complete: function() { 
                        $("#add_form").html($form1);
                        datePickerUI();
                        loadUsers();
                    $('select').material_select();
                    } 
                });



                $(".bordered").on("click", ".delete", function(e){
                    e.preventDefault();

                    $.ajax({
                        url: $(this).attr("href"),
                        type: "GET",
                        success: function(msg){
                            loadUsers();
                        }
                    });
                });
                
                datePickerUI();

                $(".bordered").on("submit", ".edit_form", function(e){
                     e.preventDefault();
                    $.ajax({
                        url: "edit.php",
                        type: "POST",
                        data: $(this).serialize(),
                        success: function(msg){
                            $(".edit_form").html(msg);
                        }
                    });
                 });

                $(".bordered").on("click", ".modal-close", function(e){
                    loadUsers();
                });

                $('#search').on('input',function(e){
                    $.ajax({
                        url: "get_users.php",
                        type: "POST",
                        data: $("#search_form").serialize(),
                        
                        success: function(rtrn){
                            $("#result").html(rtrn);
                        }
                    });
                });

                
                function loadUsers(){
                    $("#result").load("get_users.php");
                }

                function datePickerUI(){
                    $('.datepicker').pickadate({
                        selectMonths: true, // Creates a dropdown to control month
                        selectYears: 150 // Creates a dropdown of 15 years to control year
                    });
                }

                $('select').material_select();
            
            });
        </script>