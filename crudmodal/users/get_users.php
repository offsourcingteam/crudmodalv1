<?php include_once '../includes/connect.php'; ?>

<?php
	

	if($_POST){
		$search = $_POST['search'];
		$sql = "SELECT *, users.id as id, user_types.id as user_type_id FROM users INNER JOIN user_types ON users.user_type_id = user_types.id WHERE 
		firstname Like '%".$search."%'
		OR lastname Like '%".$search."%'
		OR email Like '%".$search."%'
		OR type_name Like '%".$search."%'
		OR birthdate Like '%".$search."%'
		OR users.id Like '%".$search."%'
		 ";
	}else{
		$sql = "SELECT *, users.id as id, user_types.id as user_type_id FROM users INNER JOIN user_types On users.user_type_id = user_types.id";
	}

	$result = mysqli_query($conn, $sql);

	while($row = mysqli_fetch_assoc($result)){ ?>

		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['firstname']; ?></td>
			<td><?php echo $row['lastname']; ?></td>
			<td><?php echo $row['birthdate']; ?></td>
			<td><?php echo $row['email']; ?></td>
			<td><?php echo $row['type_name']; ?></td>


			<td>

	            <a class="btn-floating btn-large waves-effect waves-light green edit btn modal-trigger" data-target="user<?php echo $row['id']; ?>"><i class="material-icons">edit</i></a>
	            <a class="btn-floating btn-large waves-effect waves-light red delete" href="delete.php?id=<?php echo $row['id']; ?>"><i class="material-icons">delete</i></a>
				<div id="user<?php echo $row['id'];?>" class="modal modal-fixed-footer">
						<div class="modal-content">
							 <h4> <i class="large material-icons">mode_edit</i></h4>
						<hr>
						<form class="edit_form">
                        <div class="edit_form_input">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input  type="text" placeholder="First Name" name="firstname" value="<?php echo $row['firstname']; ?>" class="validate"  required>
                                </div>
                                    <div class="input-field col s6">
                                    <input  type="text" name="lastname" class="validate" value="<?php echo $row['lastname']; ?>" required>
                                </div>

                                 <div class="input-field col s6">

                                    <?php 
                                        $sql_user_type = "SELECT * FROM user_types";
                                        $user_type_result = mysqli_query($conn, $sql_user_type);
                                    ?>
                                    <select name="user_type_id">
                                        <?php while($row_user_type = mysqli_fetch_assoc($user_type_result)){  ?>
                                        	<?php if($row['user_type_id'] == $row_user_type['id']){ ?>
                                            	<option value="<?php echo $row_user_type['id'] ?>" selected><?php echo $row_user_type['type_name'] ?></option>
                                            <?php }else{ ?>
                                            	<option value="<?php echo $row_user_type['id'] ?>"><?php echo $row_user_type['type_name'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <label>User Type</label>
                                </div>

                                <div class="input-field col s6">
                                    <input  id="birthdate" name="birthdate" class="datepicker" value="<?php echo $row['birthdate']; ?>" required>
                                    
                                </div>

                                <div class="input-field col s6">
                                    <input  type="email" name="email" class="validate" value="<?php echo $row['email']; ?>" required>
                                </div>
                                <input type="hidden" value="<?php echo $row['id']; ?>" name="id">


                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
					</div>
					<div class="modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
					</div>
				</div>
            </td>

            	
		

<?php
	}



?>

<script type="text/javascript">
	$(document).ready(function(){
		var $form1 = $(".edit_form").html();
		$('.modal-trigger').leanModal();

		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 150 // Creates a dropdown of 15 years to control year
		});


        $('select').material_select();
		
	});
</script>